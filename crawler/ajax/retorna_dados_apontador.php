<?php
	ini_set("display_errors", 0);
	include("../class/informacao.class.php");
	$urls = str_replace(" ", "", trim($_POST['array']));
	$urls = str_replace('\n', "", trim($_POST['array']));
	$categoria = addslashes(trim($_POST['categoria']));
	if(substr($urls, -1) == ","){
	    $urls = substr($urls, 0, -1);
	}
	$sources = explode(",", $urls);
	$i = 0;
	foreach($sources as $src){
		$src = trim($src);
		$informacao = new Informacao($src);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $src);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSLVERSION,3);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
		$data = curl_exec ($ch);
		$error = curl_error($ch);
		curl_close ($ch);

		$dom = new DOMDocument();
		$dom->loadHTML($data); //variável que contém o html da página atual

		$elements = $dom->getElementsByTagName('h1'); //pegando todos os <h1> da página
		foreach ($elements as $element) { //procurando <h1> com classe "title"
		    // $atributo = $element->getAttribute('itemprop');
		    // if($atributo == "name"){
		    	$informacao->nome = trim($element->nodeValue);
		    // }
		}

		$elements = $dom->getElementsByTagName('p'); //pegando as tags que contém os itens desejados
		foreach ($elements as $element) {
		    $stributo = $element->getAttribute('class');
		    if($stributo == "phone detail"){
		    	$informacao->telefone = trim($element->nodeValue);
		    }
		}

		$elements = $dom->getElementsByTagName('p'); //pegando as tags que contém os itens desejados
		foreach ($elements as $element) {
		    if($element->parentNode->tagName == "address"){
		    	$informacao->endereco = str_replace('
', " ", trim($element->nodeValue));
		    	$informacao->endereco = trim(str_replace("  ", " ", $informacao->endereco));
		    	$informacao->endereco = trim(str_replace("— Como chegar", "", $informacao->endereco));
		    }
		}

// 		$spans = $dom->getElementsByTagName('span'); //pegando as tags que contém os itens desejados
// 		foreach ($spans as $span) {
// 		    $stributo = $span->getAttribute('itemprop');
// 		    if($stributo == "streetAddress"){
// 		    	//echo "<span class='telefone'>".$span->getAttribute("data-content")."</span>";
// 		    	$informacao->endereco = str_replace('
// ', "", trim($span->nodeValue));
// 		    }
// 		}
		if($informacao->valida()){
			$i++;
		?>
			<div class='jumbotron' style='padding: 1%'>
				<span class="text-info br">
					[nome] =>
					<?php
						echo $informacao->nome;
					?>
				</span>
				<span class="text-info br">
					[telefone] =>
					<?php
						echo $informacao->telefone;
					?>
				</span>
				<span class="text-info br">
					[endereco] =>
					<?php
						echo $informacao->endereco;
					?>
				</span>
				<span class="text-info br">
					[url] =>
					<?php
						echo $informacao->url;
					?>
				</span>

				<span class="text-info br">
					[categoria] =>
					<?php
						echo $informacao->categoria;
					?>
				</span>
			</div>
		<?php
		// $informacao->insere();
		} else {
			print_r($informacao);
		}
	}
	echo "<h3>".$i." resultados!</h3>";