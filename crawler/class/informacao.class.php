<?php
	class Informacao{
		public $nome;
		public $telefone;
		public $endereco;
		public $url;
		public $categoria;
		function __construct($url, $categoria = NULL, $nome = NULL, $telefone = NULL, $endereco = NULL){
			$this->nome = trim($nome);
			$this->categoria = trim($categoria);
			$this->telefone = trim($telefone);
			$this->endereco = trim($endereco);
			$this->url = trim($url);
		}

		public function valida(){
			return true;
			$temp_nome = str_replace(" ", "", $this->nome);
			$what = array(" ", "(", ")", "-");
			$temp_telefone = str_replace($what, "", $this->telefone);
			if(!is_numeric($temp_telefone))
				$this->telefone = NULL;
			$temp_endereco = str_replace(" ", "", $this->endereco);
			// if($temp_nome == NULL || $temp_nome == "")
			// 	return false;
			if($temp_telefone == NULL || $temp_telefone == "")
				return false;
			// if($temp_endereco == NULL || $temp_endereco == "")
			// 	return false;
			return true;
		}

		private function remover_acentos($string) {
			$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Ê','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','_','(',')',',',';',':','|','!','"','#','$','%','&','=','?','~','^','>','<','ª','º','@' );
			$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','E','I','O','U','n','n','c','C','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-','-' );
			return utf8_decode(str_replace($what, $by, $string));
		}

		public function inserir_banco($post){
			include "bd.php";
			if($banco) {
				if(mysqli_query($banco, "INSERT INTO paloma_posts (post_content, post_title, post_name, guid, post_type, post_status, post_date) VALUES ('$post->post_content', '$post->post_title', '$post->post_name', '$post->guid', '$post->post_type', '$post->post_status', '$post->post_date')")){
					$inserido = mysqli_insert_id($banco);
					if(mysqli_query($banco, "INSERT INTO paloma_term_relationships (object_id, term_taxonomy_id, term_order) VALUES ($inserido, $post->categoria, 0)")){
						mysqli_query($banco, "UPDATE paloma_term_taxonomy SET count = 1 WHERE term_taxonomy_id = $post->categoria");
						return true;
					}
					else {
						echo "Erro ao inserir categoria";
						return false;
					}
				} else {
					return false;
				}
			} else {
				echo "Erro ao conectar no banco de dados!".mysqli_errno($banco);
				return false;
			}
		}

		public function insere(){
			$this->telefone = addslashes(trim($this->telefone));
			$this->nome = addslashes(trim($this->nome));
			$this->endereco = addslashes(trim($this->endereco));
			$this->url = addslashes(trim($this->url));

			$this->post_content = utf8_decode($this->endereco."<br />".$this->telefone);
			$this->post_title = utf8_decode($this->nome);
			$this->post_name = $this->remover_acentos($this->nome);
			$this->guid = "http://www.telefones.com.br/".$this->post_name; // colocando o titulo com url amigavel no final da url
			$this->post_type = "post";
			$this->post_status = "publish";
			$this->post_date = date("Y-m-d H:m:s");

			// if($this->inserir_banco($this)) //comentado para não realizar a inserção
			// 	echo "Inserido com sucesso!";
			// else
			// 	echo "Erro ao inserir no banco de dados!";
		}
	}