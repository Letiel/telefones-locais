<!DOCTYPE html>
<html>
	<head>
		<title>Letiel.test</title>
		<meta charset='utf-8' />
		<!-- <link rel="stylesheet" type="text/css" href="/css/bootstrap.css"> -->
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<!-- // <script type="text/javascript" src='/js/jquery-2.1.4.min.js'></script> -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<!-- // <script type="text/javascript" src='/js/bootstrap.min.js'></script> -->
		<script type="text/javascript" src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
		<style type="text/css">
			.br{
				display: block;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
		<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
	</head>
	<body>
		<div class="row panel panel-default col-md-10 col-md-offset-1">
			<div class="panel-body">
				<form action='#' method='post'>
					<div class="form-group">
						<input required='required' class="form-control" id='url_cidade' />
					</div>

					<div class="form-group">
						<?php include 'class/bd.php';
								if(!$banco)
									echo "Não conectou no banco de dados!"; ?>
						<select required='required' id="cidade" class='form-control'>
							<option value=''>Selecione</option>
							<?php
								$query = mysqli_query($banco, "SELECT * FROM paloma_terms JOIN paloma_term_taxonomy ON (paloma_term_taxonomy.term_id = paloma_terms.term_id) WHERE paloma_term_taxonomy.parent = 0");
								while($categoria = mysqli_fetch_array($query)){
									echo utf8_encode("<option value='$categoria[term_id]'>$categoria[name]</option>");
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<?php
							include 'class/bd2.php';
							$q2 = mysqli_fetch_object(mysqli_query($banco2, "SELECT ocupado FROM crawler WHERE id = 1"));
							// echo $q2->ocupado;
							if($q2->ocupado == 1)
								$permitir = "disabled";
							else
								$permitir = "";

						?>
						<button id='submit' class="btn btn-primary" <?php echo $permitir ?> type='submit'>Enviar</button>
						<button class="btn btn-default" id='total_cadastros' disabled>______</button>
						<a id="ocupado" class='btn btn-default'></a>
					</div>
				</form>
			</div>
		</div>
		<div class="row panel panel-default col-md-10 col-md-offset-1">
			<div class="panel-body">
				<div class='alert alert-info' id='aguarde' style='display: none;'>
					<b>Aguarde...</b>
					<img src='/crawler/img/loading.gif' style='width: 170px;' />
				</div>
			</div>
			<div id="resultado"></div>
		</div>
		<div class="row panel panel-default col-md-10 col-md-offset-1">
			<div class='panel-body'>
				<p>Último:</p>
				<p id="ultimo_cadastrado"></p>
			</div>
		</div>
		
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

		<script type="text/javascript">

			function loop_total(){
				$.post("/crawler/loop_total.php", {

				}, function(result){
					$("#total_cadastros").html(result);
					document.title = result;
				});

				$.post("/crawler/ultimo_cadastrado.php", {

				}, function(result){
					$("#ultimo_cadastrado").html(result);
				});

				$.post("/crawler/ocupado.php", {

				}, function(result){
					if(result.ocupado == 1){
						classe = "btn-danger";
						text = "Ocupado!";
						$("#submit").prop("disabled","disabled");
					}
					else{
						classe = "btn-success";
						text = "Liberado";
						$("#submit").prop("disabled","");
					}
					$("#ocupado").html(text);
					$("#ocupado").removeClass("btn-success");
					$("#ocupado").removeClass("btn-danger");
					$("#ocupado").addClass(classe);
				}, 'json');
			}

			setInterval(loop_total, 3000);

			$(document).ready(function(){
				$("#ocupado").click(function(){
					$.post("/crawler/ocupar.php", {
					}, function(result){
					});
				});

				$('select').select2();
				$('form').submit(function(){
					console.log($("#url_cidade").val());
					console.log($("#cidade").val());
					$("#aguarde").show();
					$.post("/crawler/crawler_geral.php", 
					{
						url_cidade: $("#url_cidade").val(),
						cidade: $("#cidade").val(),
					},
					function(result){
							$('#cidade').prop('selectedIndex',0);
							$("#url_cidade").val("");
				        	$("#resultado").html(result);
				        	$("#aguarde").hide();
				    });	

					// $.ajax({
					//   method: "POST",
					//   url: "/letiel/crawler_geral.php",
					//   timeout: 0,
					//   async: true,
					//   data: {
					//   	url_cidade: $("#url_cidade").val(),
					//   	cidade: $("#cidade").val(),
					//   }
					// })
					//   .done(function( result ) {
		   //  			$('#cidade').prop('selectedIndex',0);
		   //  			$("#url_cidade").val("");
		   //          	$("#resultado").html(result);
		   //          	$("#aguarde").hide();
					//   });

					return false;
				});
			});
		</script>
	</body>
</html>